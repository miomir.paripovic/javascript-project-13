# Project no. 13

## Task

This is my assignment for week no. 13 and my third JavaScript project. The main goal for this week is to recreate a simple calculator by using objects and prototypes. 'onclick' method is not allowed. If possible, try not to use eval function.

The calculator should support these operations:

- addition
- multiplication
- division
- subtraction
- backwards option
- ability to enter values only by keyboard
- basic memory operations (to the memory, clear the memory)