const Calculator = function() {
    this.calculatorMemory = document.getElementById("screen");   

    this.OPERATORS = ['+', '-', '*', '/']
    this.OPERATORS_DECPOINT = ['+', '-', '*', '/', '.']
    // keypad (keydown) values for *, +, -, ., /
    this.KEY_CODES =[42, 43, 45, 46, 47];    
    
    this.evalFlag = true;
    this.saveValueToMemory = '0';
}

Calculator.prototype.putOnScreen = function(aValue) {
    // check if digit can fit on the screen (maximum 19)
    if (this.calculatorMemory.value.length > 18) {
        this.evalFlag = false;
        this.calculatorMemory.value = '0';
        alert("Error. Too many digits!");
        return false;
    }
        // at start when 0 or result on screen
        if (this.calculatorMemory.value === '0' || this.evalFlag) {
            // if numbers follow, replace zero (no leading zero) using includes method
            if (!this.OPERATORS_DECPOINT.includes(aValue)) {
                this.calculatorMemory.value = aValue;
            // if operators/dec. point follow, keep zero or value on the screen
            } else {
                this.calculatorMemory.value += aValue;
            }
        // this part checks for two operators/decimal point one after another
        } else if (this.OPERATORS_DECPOINT.includes(aValue)) {
            let aLength = this.calculatorMemory.value.length;
            if (aLength > 0 && this.OPERATORS_DECPOINT.includes(this.calculatorMemory.value[aLength-1])) {
                this.calculatorMemory.value = this.calculatorMemory.value.slice(0, -1) + aValue;                
            } else {
                this.calculatorMemory.value += aValue;
            }
        // numbers
        } else {
            this.calculatorMemory.value += aValue;
        }        
        this.evalFlag = false;
        return true;
}

Calculator.prototype.calculateResult = function() {
    console.log("before", this.calculatorMemory.value);
    // eliminates extra operator
    if (this.OPERATORS.includes(this.calculatorMemory.value[this.calculatorMemory.value.length-1])) {
        // slice string method, return new string: from zero to second last
        this.calculatorMemory.value = this.calculatorMemory.value.slice(0, -1);
    }
    // try if everything good; throw error if problem
    try {
        // eval function transforms string to result
        this.calculatorMemory.value = eval(this.calculatorMemory.value);
        console.log("after", this.calculatorMemory.value);
        this.evalFlag = true;
    }
    catch (error) {
        alert("Error. Probably too many dots.");
        return false;
    }
    return true;
}

Calculator.prototype.calculatorReset = function() {
    this.calculatorMemory.value = '0';
    this.evalFlag = false;
    return true;
}

Calculator.prototype.backwards = function() {
    // two characters or more, cut the last
    if (this.calculatorMemory.value.length > 1) {
        this.calculatorMemory.value = this.calculatorMemory.value.slice(0, -1);
    } else {
        this.calculatorMemory.value = 0;
    }
    return true;
}

Calculator.prototype.memoryFunction = function(aValue) {
    switch (aValue) {
        case 'm':
            if (this.calculatorMemory.value !== '0') {
                // place to memory
                this.saveValueToMemory = this.calculatorMemory.value;
                document.getElementById("screen").style.background = "papayawhip";
            } else {
                // erase from memory
                this.saveValueToMemory = '0';
                document.getElementById("screen").style.background = "white";
            }
            this.evalFlag = true;
            break;
        // return from memory on to screen
        case 'rm':
            if (this.saveValueToMemory !== '0') {
                if (this.OPERATORS.includes(this.calculatorMemory.value[this.calculatorMemory.value.length-1])) {
                    this.calculatorMemory.value += eval(this.saveValueToMemory);
                } else {
                    this.calculatorMemory.value = eval(this.saveValueToMemory);
                }
            } else {
                alert("There is nothing in memory.");
                return false;
            }
            this.evalFlag = true;
            break;
    }
    return true;
}

let calculator = new Calculator();
document.addEventListener("keypress", checkKeyDown);

function checkKeyDown(e) {
    let aChar = '';
    console.log("event", e);
    console.log("keyboard code", e.keyCode);
    if (e.keyCode > 47 && e.keyCode < 58) {        
        aChar = String.fromCharCode(e.keyCode);
        console.log(aChar);
        console.log(typeof(aChar));
        calculator.putOnScreen(aChar);
    } else if (calculator.KEY_CODES.includes(e.keyCode)) {
        aChar = String.fromCharCode(e.keyCode);
        console.log(aChar);
        console.log(typeof(aChar));
        calculator.putOnScreen(aChar);
    } else if (e.keyCode === 13 || e.keyCode === 187) {
        calculator.calculateResult();
    } else if (e.keyCode === 98 || e.keyCode === 39) {
        calculator.backwards();        
    } else if (e.keyCode === 99) {
        calculator.calculatorReset();
    } else if (e.keyCode === 109) {
        calculator.memoryFunction('m');
    } else if (e.keyCode === 114) {
        calculator.memoryFunction('rm');
    }
    return true;
}